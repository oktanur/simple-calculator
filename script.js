let screenDisplay = [];
let joinString;
let button = document.querySelectorAll(".button");
let screen = document.getElementById("screenDisplay");
let percentage = document.getElementById("percentage");

// update display
const updateDisplay = (ele) => {
  if (ele.innerText === "." && screenDisplay[screenDisplay.length - 1] === ".")
    return;
  else if (
    /[+*\/-]?[0-9]+\.[0-9]+$/.test(joinString) &&
    ele.innerText === "."
  ) {
    return;
  } else if (/[+*\/-]$/.test(joinString) && ele.innerText === "/") {
    return;
  } else if (/[+*\/-]$/.test(joinString) && ele.innerText === "*") {
    return;
  } else if (/[+*\/-]$/.test(joinString) && ele.innerText === "-") {
    return;
  } else if (/[+*\/-]$/.test(joinString) && ele.innerText === "+") {
    return;
  }
  let buttonValue = ele.innerText;
  screenDisplay.push(buttonValue);
  joinString = screenDisplay.join("");
  screen.innerText = joinString;
};

// function if calculator button is clicked
const buttonClicked = () => {
  button.forEach((ele) => {
    ele.onclick = () => {
      if (ele.id === "clear") {
        screenDisplay = [];
        screen.innerText = "";
      } else if (ele.id === "equal") {
        screen.innerText = joinString + "\n" + "=" + eval(joinString);
      } else if (ele.id === "delete") {
        joinString = joinString.slice(0, -1);
        screen.innerText = joinString;
        if (joinString.length == 0) {
          screenDisplay = [];
          screen.innerText = "";
        } else {
          screenDisplay = screenDisplay.slice(0, -1);
        }
      } else {
        updateDisplay(ele);
      }
      console.log(joinString);
    };
  });
};

buttonClicked();
